![Logo](https://i.imgur.com/VgJJ5ty.png)

# Nativegames 2.0

Nativegames is a new unblocked games site that has a static proxy, loads of games, a sleek look, and much much more for you to see yourself!<br/><br/>
![GitHub repo size](https://img.shields.io/github/repo-size/parcoil/nativegames.net?color=33B3DB&label=Totall%20Size)

# Domains 
[The list of Domains can be found Here](https://discord.gg/k7jzF4jFpr)

## Deployment
Nativegames is a static site and can be easily deployed on platforms like GitHub Pages and Glitch.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https%3A%2F%2Fgithub.com%2FParcoil%2Fnativegames.net)  
[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/Parcoil/nativegames.net)

## Demo
[nativegames.net](https://nativegames.net)

## Authors
- [@Thedogecraft](https://github.com/Thedogecraft)
- [@Minoa](https://github.com/MinoaBaccus)
- [@Noober](https://github.com/Hackerman2763)
- [@3kh0](https://github.com/3kh0)
